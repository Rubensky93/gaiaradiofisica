package com.ruletadefortuna;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.sql.SQLException;


public class SectorEdit extends Activity {

    private SectorsDbAdapter mDbHelper;
    private EditText mSectorText;
    private Long mRowId;
    private int mPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.edit_sector);
        setTitle(R.string.config);

        mSectorText = (EditText) findViewById(R.id.sector_text);
        Button buttonOk = (Button) findViewById(R.id.button_ok);
        Button buttonDelete = (Button) findViewById(R.id.button_delete);

        mDbHelper = new SectorsDbAdapter(this);
        try {
            mDbHelper.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Bundle extras = getIntent().getExtras();
        mRowId = extras != null ? extras.getLong(SectorsDbAdapter.KEY_ROWID)
                : null;
        mPosition = extras != null ? extras.getInt(SectorsConfig.LIST_POSITION)
                : null;
        try {
            populateText();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        buttonOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on clicks
                saveState();
                setResult(RESULT_OK);
                finish();
            }
        });
        if (mPosition < 2){
            buttonDelete.setEnabled(false);
        }
        else
        {
            buttonDelete.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Perform action on clicks
                    mDbHelper.deleteSector(mRowId);
                    setResult(RESULT_OK);
                    finish();
                }
            });
        }

    }
    @Override
    protected void onPause() {
        super.onPause();
        saveState();
    }
    @Override
    protected void onResume() {
        super.onResume();
        try {
            populateText();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveState();
        outState.putSerializable(SectorsDbAdapter.KEY_ROWID, mRowId);
    }

    private void populateText() throws SQLException {
        if (mRowId != null) {
            Cursor sector = mDbHelper.fetchSector(mRowId);
            startManagingCursor(sector);
            mSectorText.setText(sector.getString(
                    sector.getColumnIndexOrThrow(SectorsDbAdapter.KEY_BODY)));
        }
    }

    private void saveState() {
        String body = mSectorText.getText().toString();
        if (body.length() != 0 && mRowId != null) {
            mDbHelper.updateSector(mRowId, body);
        }
    }
}
