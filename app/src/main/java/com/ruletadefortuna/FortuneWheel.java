package com.ruletadefortuna;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;


public class FortuneWheel extends Activity {

    private RouletteView mRouletteView;
    private Button btnInfo;
    private FragmentManager fm;
    public static final int CONFIG_ID = Menu.FIRST;
    private SQLiteRuletaHelper admin;

    /** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mRouletteView = (RouletteView) findViewById(R.id.rouletteView);
        admin = new SQLiteRuletaHelper(this, "ruleta", null, 1);
        SQLiteDatabase db= admin.getWritableDatabase();
        fm = getFragmentManager();


        Typeface font = Typeface.createFromAsset( getAssets(), "fontawesome-webfont.ttf" );
        btnInfo = (Button) findViewById(R.id.info);
        btnInfo.setTypeface(font);
        //quitar bordes al boton
        btnInfo.setBackgroundResource(0);

        btnInfo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                DFragment dFragment = new DFragment();
                // Show DialogFragment
                dFragment.show(fm, "Dialog Fragment");
            }
        });


        /*db.execSQL("INSERT INTO pregunta VALUES (1, 'cuanto es 1+1',3,'1','3','2','5','4',1)");
        db.execSQL("INSERT INTO pregunta VALUES (2, 'cuanto es 2+3', 4,'1','3','2','5','4',2)");
        db.execSQL("INSERT INTO pregunta VALUES (3, 'cuanto es 1+2', 2,'1','3','2','5','4',3)");
        db.execSQL("INSERT INTO pregunta VALUES (4, 'cuanto es 1+3', 5,'1','3','2','5','4',4)");*/

    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mRouletteView.updateSectors();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        try {
            mRouletteView.updateSectors();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, CONFIG_ID, 0, R.string.menu_config);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case CONFIG_ID:
                Intent i = new Intent(this, SectorsConfig.class);
                startActivity(i);
                return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    public void mostrarPregunta(View v){


        Bundle b = new Bundle();
        SQLiteDatabase db= admin.getReadableDatabase();
        String[]valores = new String[]{String.valueOf(((int)(Math.random()*4+1)))};
        Cursor c = db.rawQuery("SELECT * FROM pregunta WHERE idgrupo = ?",valores);

        if(c.moveToFirst()){

            do{

                /*p.setPregunta(c.getString(1));
                p.setRespuestaCorrecta(c.getInt(2));
                p.setRespuesta1(c.getString(3));
                p.setRespuesta2(c.getString(4));
                p.setRespuesta3(c.getString(5));
                p.setRespuesta4(c.getString(6));
                p.setRespuesta5(c.getString(7));
                p.setIdgrupo(c.getInt(8));*/

                b.putString("pregunta", c.getString(1));
                b.putInt("respuestaCorrecta", c.getInt(2));
                b.putString("resp1", c.getString(3));
                b.putString("resp2", c.getString(4));
                b.putString("resp3", c.getString(5));
                b.putString("resp4", c.getString(6));
                b.putString("resp5", c.getString(7));
                b.putInt("idgrupo", c.getInt(8));


            }while(c.moveToNext());

        }

        int r = (int)(Math.random()*500+1200);
        mRouletteView.spin(r);
        Toast.makeText(FortuneWheel.this, "Force: " + r  + "  ---  Sector: "+ mRouletteView.getCurrentSector(), Toast.LENGTH_SHORT).show();
      /*  try {

            Thread.sleep(800);
            Intent i = new Intent(this,PantallaPregunta.class);
            i.putExtras(b);
            startActivity(i);
        } catch (InterruptedException e) {
            e.printStackTrace();*/
        }
    public void mostrarStats(View v){
        Intent i = new Intent(FortuneWheel.this,PantallaStats.class);
        startActivity(i);
    }
    }

//}
