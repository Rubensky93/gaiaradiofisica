package com.ruletadefortuna;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteRuletaHelper extends SQLiteOpenHelper {

    String sql="create table pregunta (idpregunta INTEGER PRIMARY KEY ,"
            + " pregunta TEXT,"
            + " respuesta_correcta INTEGER,"
            + " resp1 TEXT,resp2 TEXT,resp3 TEXT,resp4 TEXT,resp5 TEXT,"
            + " idgrupo INTEGER)";

    public SQLiteRuletaHelper(Context contexto, String nombre, SQLiteDatabase.CursorFactory factory,
                              int version) {
        super(contexto, nombre, factory, version);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS pregunta");
        db.execSQL(sql);

    }

}
