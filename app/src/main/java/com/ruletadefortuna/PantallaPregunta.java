package com.ruletadefortuna;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


public class PantallaPregunta extends Activity {

    private TextView tvPregunta;
    private RadioGroup rg;
    private RadioButton rb1, rb2, rb3, rb4, rb5;
    private Button btnComprobar;
    int num = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_pregunta);

        btnComprobar = (Button) findViewById(R.id.btnComprobar);

        tvPregunta = (TextView) findViewById(R.id.tvPregunta);

        rb1 = (RadioButton) findViewById(R.id.resp1);
        rb2 = (RadioButton) findViewById(R.id.resp2);
        rb3 = (RadioButton) findViewById(R.id.resp3);
        rb4 = (RadioButton) findViewById(R.id.resp4);
        rb5 = (RadioButton) findViewById(R.id.resp5);
        rg = (RadioGroup) findViewById(R.id.rgRespuestas);

        Bundle b = getIntent().getExtras();

        tvPregunta.setText(b.getString("pregunta"));

        rb1.setText(b.getString("resp1"));
        rb2.setText(b.getString("resp2"));
        rb3.setText(b.getString("resp3"));
        rb4.setText(b.getString("resp4"));
        rb5.setText(b.getString("resp5"));


        final int respCorrecta = b.getInt("respuestaCorrecta");

        btnComprobar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    if(rb1.isChecked()){
                        num = 1;
                    }else if(rb2.isChecked()){
                        num = 2;
                    }else if(rb3.isChecked()){
                        num = 3;
                    }else if(rb4.isChecked()){
                        num = 4;
                    }else if(rb5.isChecked()){
                        num = 5;
                    }else {
                        Toast.makeText(PantallaPregunta.this, "No hay nada seleccionado", Toast.LENGTH_SHORT).show();
                }

                if (num == respCorrecta){
                    Toast.makeText(PantallaPregunta.this, "HAS ACERTADO!", Toast.LENGTH_SHORT).show();

                }else{
                    Toast.makeText(PantallaPregunta.this, "HAS FALLADO!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

}